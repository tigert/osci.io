---
title: Infrastructure and Services
---

## Introduction

### Goal

OSAS helps maintain the *Community Cage* project, which provides co-location services to various community projects (CentOS, Fedora, Gluster, Ceph…).

To provide infrastructure for more granular needs, or for smaller communities without their own hardware infrastructure, OSAS created the OSCI project. OSCI is a tenant of the Community cage and provide:

* shared services, used by multiples communities
* hosted services for small tenants or tenants in need of complementary services

All services are made using Free software.

### Provided Services

The list of provided services is flexible and expanding with time and new projects.

Here is a non-exhaustive list of services we are used to provide and can deploy in a reasonable time:

| Category | Service Description |
|:--------:|:---:|
| DNS      | secondary DNS servers for community domains (transfers secured using TSIG) |
| Mail     | redirections, boxes with IMAPS/POP3S access (with anti-virus and anti-SPAM) |
|          | mailing-lists (using Mailman 3, migration from Mailman 2 possible) |
|          | secondary MXs for community domains |
| Web      | web sites hosting (static preferred, Ruby/Python/PHP/NodeJS possible)<br/>with HTTPS (using Let's Encrypt) and security settings (headers, CSP…) |
|          | builders for various static sites generators (Ascii Binder, Jekyll, Middleman, Planet) |
| Tickets  | RT queue on our shared server |
| Time     | time server (NTP) accessible inside the Community Cage |
| VM       | Raw VM with root access |

## Services Administration

Shared services are fully managed by the OSAS Community Infrastructure (ComInfra) team, whereas hosted projects are handled according to project members' wishes, from raw VM with root access to full management by the OSAS team and anything in the middle.

The infrastructure "code" is public and we much welcome contributions. We use Ansible for deployment and maintain various Ansible roles on GitLab/GitHub already. These services are deployed using the [OSAS/community-cage-infra-ansible repository](https://gitlab.com/osas/community-cage-infra-ansible) on GitLab.

## Current Services

### Shared Services

| Service        | Location                           | Description                                                             | Status |
|:--------------:|:----------------------------------:|:-----------------------------------------------------------------------:|:------:|
| MX1            | polly.osci.io                      | Mail redirections, RT mail accounts                                     | OK     |
| MX2            | polly.osci.io<br/>francine.osci.io | backup mail server for OSCI and communities (oVirt)                     | OK     |
| NS1            | polly.osci.io                      | DNS master server                                                       | OK     |
| NS2            | polly.osci.io<br/>francine.osci.io | DNS slave server for OSCI and communities (oVirt)                       | OK     |
| NTP            | ntp1.osci.io<br/>ntp2.osci.io      | NTP stratum 2 server using a CDMA device                                | OK     |
| PXE            | speedy.osci.io<br/>pxe.osci.io     | DHCP/TFTP, netboot menu for various distros, manual/unattended upgrades | OK     |
| Ticket Tracker | tickets.osci.io                    | RT for OSCI and communities (RDO Cloud, Fedora Infra Security)          | OK     |
| VMs            | speedy.osci.io<br/>guido.osci.io   | hypervisor                                                              | OK     |
| VMs            | jerry.osci.io<br/>seymour.osci.io  | hypervisor                                                              | WIP    |

### Hosted/Managed Services

|                   |                   |                                          |                                                                   |  Level of     |        |
| Tenant            | Service           | Location                                 | Description                                                       | Involvement   | Status |
|:-------------------------:|:----------------------------------------:|:-----------------------------------------------------------------:|:-------------:|:------:|
| ASCII Binder        | Website Builder   |                                          | Webserver                                                         | full          | WIP    |
|                     | Website           |                                          | ASCII Binder builder                                              | full          | WIP    |
| Community           | Website Builder   | osas-community-web-builder.int.osci.io   | Middleman builder for community.redhat.com                        | full          | OK     |
|                     | Website           | community.redhat.com                     | Webserver for community.redhat.com                                | full          | OK     |
| Gnocchi             | Website           | gnocchi.osci.io                          | Gnocchi & client documentation builder and web server, web server | full          | OK     |
| Minishift           | MLs               | lists.minishift.io                       | Mailman 3                                                         | full          | OK     |
| NFS Ganesha         | MLs               | lists.nfs-ganesha.org                    | Mailman 3                                                         | full          | OK     |
| Open Source Infra   | MLs               | lists.opensourceinfra.org                | Mailman 3                                                         | full          | OK     |
| OpenJDK             | Sources           | openjdk-sources.osci.io                  | Webserver and SFTP accounts                                       | full          | OK     |
| oVirt               | MLs               | mail.ovirt.org                           | Mailman 3, web UI on lists.ovirt.org                              | full          | OK     |
|                     | Website Builder   | ovirt-web-builder.int.osci.io            | Middleman builder for www.ovirt.org                               | full          | OK     |
| PCP                 | Tools             |                                          | Taskboard and various tools                                       | hosting-only  | OK     |
| PO4A                | Website           | www.po4a.org                             | Jekyll or ASCII Binder builder and webserver                      | full          | OK     |
|                     | MLs               |                                          | Mailman 3                                                         | full          | OK     |
| Pulp                | Publishing Server | pulp-repo.osci.io                        | Pulp serving Pulp                                                 | hosting-only  | OK     |
|                     | Website Builder   | pulp-web-builder.int.osci.io             | Jekyll builder for pulpproject.org                                | collaboration | OK     |
|                     | Websites          | pulpproject.org<br/>docs.pulpproject.org | Webserver for project pages and documentation                     | collaboration | OK     |
| RDO                 | Website Builder   | rdo-web-builder.int.osci.io              | Middleman builder for www.rdoproject.org                          | full          | OK     |
|                     | MLs               | lists.rdoproject.org                     | Mailman 2                                                         | full          | OK     |
|                     | MLs               | (future lists.rdoproject.org)            | Mailman 3                                                         | full          | WIP    |
| SCL                 | Website           | (scl-web.osci.io)                        | Webserver                                                         | hosting-only  | OK     |
| Spice               | Website           | www.spice-space.org                      | Webserver                                                         | full          | OK     |
| The Open Source Way | Website           | theopensourceway.org                     | Mediawiki                                                         | full          | OK     |

