---
title: Hardware List
---

## Rackable Equipments

| Name        | Location        | Size | Network Interfaces | Access       | Description                   | Purpose       |
|:-----------:|:---------------:|:----:|:------------------:|:-------------|:------------------------------|:--------------|
| Catatonic   | RDU2 3C-B04 U01 |  6U  | CMM1/CMM2: OSAS-Management<br/>A1/A2: Ex0/60 trunk OSAS-* | CMM master: cmm-catatonic.adm.osci.io<br/>A1: switch-a1-catatonic.adm.osci.io<br/>A2: switch-a2-catatonic.adm.osci.io | Supermicro Microblade MBE-628E-816 with 2 redundant management modules MBM-CMM-001 and two independant network modules MBM-XEM-002 | Fedora Atomic, OSCI hypervisors/backup/monitoring
| Conserve    | RDU2 3C-B02 U21 |  1U  | eth0: OSAS-Management | conserve.adm.osci.io | Digi CM console server | Emergency console access |
| Guido       | RDU2 3C-B02 U19 |  1U  | eth0: OSAS-Public<br/>eth1: trunk OSAS-Provisioning/Management/Internal | guido.adm.osci.io | IBM System x3550 M2, 4x Intel Xeon E5530 2.40GHz, 34GB RAM, LSILOGIC SAS1068E 160GB RAID 1, 600GB soft RAID 1 with 2 spares | OSCI hypervisor |
| Speedy      | RDU2 3C-B02 U20 |  1U  | eth0: OSAS-Public<br/>eth1: trunk OSAS-Provisioning/Management/Internal | speedy.adm.osci.io | IBM System x3550 M2, 4x Intel Xeon E5530 2.40GHz, 34GB RAM, LSILOGIC SAS1068E 160GB RAID 1, 600GB soft RAID 1 with 2 spares    | OSCI hypervisor |
| TempusFugit | RDU2 3C-B02 U38 |  1U  | eth0: OSAS-Internal | tempusfugit.int.osci.io | Tempus LX CDMA | Secure time provider

(the U indicated in the location is the bottom one where the equipment seats)

## Blades

| Name            | Server    | Location    | Network Interfaces | Purpose                   |
|:---------------:|:---------:|:-----------:|:------------------:|:--------------------------|
| Seymour         | Catatonic | A1          | admin: 172.24.31.20<br/>eth0+eth1: OSAS-Provisioning<br/>eth2+eth3: OSAS-Public,OSAS-Management,OSAS-Internal | OSCI Hypervisor |
| Jerry           | Catatonic | A2          | admin: 172.24.31.22<br/>eth0+eth1: OSAS-Provisioning<br/>eth2+eth3: OSAS-Public,OSAS-Management,OSAS-Internal | OSCI Hypervisor |
| fedora-atomic-1 | Catatonic | A5          | admin: 172.24.31.24<br/>eth0+eth1: OSAS-Provisioning<br/>eth2+eth3: OSAS-Public | Fedora Atomic control node |
| fedora-atomic-1 | Catatonic | A6          | admin: 172.24.31.25<br/>eth0+eth1: OSAS-Provisioning<br/>eth2+eth3: OSAS-Public | Fedora Atomic control node |
| fedora-atomic-1 | Catatonic | A7          | admin: 172.24.31.26<br/>eth0+eth1: OSAS-Provisioning<br/>eth2+eth3: OSAS-Public | Fedora Atomic test node |
| fedora-atomic-1 | Catatonic | A8          | admin: 172.24.31.27<br/>eth0+eth1: OSAS-Provisioning<br/>eth2+eth3: OSAS-Public | Fedora Atomic test node |
| fedora-atomic-1 | Catatonic | A9          | admin: 172.24.31.28<br/>eth0+eth1: OSAS-Provisioning<br/>eth2+eth3: OSAS-Public | Fedora Atomic test node |
| fedora-atomic-1 | Catatonic | A10         | admin: 172.24.31.29<br/>eth0+eth1: OSAS-Provisioning<br/>eth2+eth3: OSAS-Public | Fedora Atomic test node |

