---
title: Infra RPM Repository
---

## Goal

When a software is not yet packaged, or a quick fix is needed, we may create a RPM package. We try to push these changes in the distribution but as it may take some time to be integrated properly, this repository acts as a stop-gap. When integrated or becoming obsolete the packages will be removed.

You may use this repository freely, but beware it is only meant to allow clean and easy deployment in our infrastructure and we offer no warranty.

## Access

Our repository is made using [Copr](https://copr.fedorainfracloud.org/), the [Fedora](https://getfedora.org/) community build service.

You can find the necessary YUM/DNF configuration on [our Repository Page](https://copr.fedorainfracloud.org/coprs/duck/osas-infra-team-rpm-repo/). Please note the method involving `yum-plugin-copr` [is unfortunately not usable](https://bugzilla.redhat.com/show_bug.cgi?id=1469172).

We also have an [additional testing repository](https://copr.fedorainfracloud.org/coprs/duck/osas-infra-team-rpm-repo-devel/) used to test changed before publishing a final version.

## Contributing

We welcome feedback and suggestions (bug reports, pull requests…).

The sources used to make the packages can be found in the [RPM git repository](https://gitlab.com/osas/osas-infra-team-rpm-pkg.git).
For each package a `README.md` file document the software and the reason to package it. The RPM specfile, along with extra sources or patches, are sufficient for anyone to rebuild the package.

## Workflow

### Adding a New Package

Each package is defined in its own subdirectory. Don't forget to document the package in the `README.md` file.

In the Copr's interface, choose the `SCM` build method, fill-in the `Type` (Git), VCS `Clone url` (above), and only add the subdirectory (Copr will find out the rest by itself). Also, the `Webhook rebuild` needs to be activated to trigger build automagically.

### Development and Publishing

Changes go through the git repository in the `master` branch and test builds (in the test repository) are triggered by pushing your git commits. There is no support for using topic branches at the moment.

On a test host with our repositories configured (but testing repository should be disabled by default), you can test your package using:

```shell
    yum clean all
    yum --enablerepo=osas_infra_devel install <package>
```

You can repeat the process until you're satisfied.

To publish a package, tag the final commit with the package name and full version (NVR without `dist`) and push, this will trigger the build in the production repository.

## Problems

We have identified a few bugs impacting the build:

* [webhook should not trigger duplicate build](https://bugzilla.redhat.com/show_bug.cgi?id=1484203): can be ignored, or duplicate builds can be canceled
* [build stops after source package is built](https://bugzilla.redhat.com/show_bug.cgi?id=1484207): build needs to be restarted
* [multiple build of the same version publishes all versions](https://bugzilla.redhat.com/show_bug.cgi?id=1489550): previous builds of the same exact versions need to be deleted

