---
title: Blade OS Setup
---

## Introduction

After an installation, an automatic one especially, a Blade needs proper basic settings before being provisioned like any other hosts via Ansible common rule.

Blades are in a specific network environment with the two microblade internal switches, so a specific configuration is needed.

The goal is to ensure:

* defining the blade's hostname (most needed after an automatic installation using a generic name)
* setup of the network, taking advantage of the switches redundancy (bonding using active-backup ; balance-alb was tested but did not work), with VLAN and bridging support

The playbook fetches the Blade's hardware information to get the MAC address of the first interface, search it in the DHCP server's ARP table to get the dynamic IP, and create a temporary Ansible host to connect to. Then it computes the settings and apply them.

## Limitations and Todolist

### Blades only

The script has be made more generic, but we need to fetch the list of MACs to generate the configuration and that's not done yet. To setup Blades we use the OEM information as we need it to discover the dynamic IP on the OSAS-Provisioning VLAN, but that's not applicable other type of hosts.

### Bonding Only

We need bonding for redundancy, so at the moment independent interfaces are not (yet) configured.

### Generation Method / Red Hat Systems Only

Unfortunately the `nmcli` Ansible module is plagued by multiple bugs and is unusable. We thought about issuing the command directly but it is fragile. Also when this playbook was started we needed support for Fedora Atomic, and at that time (version 26) there was no Network manager support. So we decided to generate configuration files directly for the needed OSes.

This means the playbook can currently only provision OSes from the RedHat family. Using different templates it should not be difficult to add support for Debian-based OSes in the future.

Starting from CentOS 8 (to get a more recent version of systemd) we might reconsider using systemd-networkd. As support for Fedora Atomic 26 was dropped, using Network Manager might also be an option.

## Blade IP definition

A few settings are needed to guide the Ansible playbook to compute the settings.

Here is an example you would store in `host_vars/<bladename>/net.yml` (adding to the list of interfaces):


```yaml
---
net:
  ifaces:
    …
  ips:
    'OSAS-Internal': 172.24.32.6
    'OSAS-Management': 172.24.31.6
    'OSAS-Public': 8.43.85.207
  bonding:
    0: [2, 3]
```

The `bladename` is used to define the hostname.

`ips` defines the blade's IP for each VLAN it is part of. There is no need to define a netmask/gateway/… as these parameters are part of the VLAN global definitions in `group_vars/all/network.yml`.

`bonding` defines the list of bonding interfaces. The key defines the interface name (`bond<key>`) and the value is the interface pair.


## Generating Blade Configuration

When settings are ready, just fire the playbook:

```shell
 ansible-playbook --diff -l <bladename> playbooks/supermicro_blade_setup.yml

```

If the playbook says:

* `Could not find MAC address`: either the blade position is wrong and there is no blade inserted, or there is a bug in the playbook
* `Could not find IP address (not in ARP table)`: most probably the ARP entry expired, so you would need to trigger some network communication with Speedy (console login + DHCP client restart, reboot, ping dynamic IP from Speedy…)
