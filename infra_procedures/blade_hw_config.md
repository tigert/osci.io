---
title: Blade Hardware Configuration
---

## Declaring the blade in Ansible

The blade is declared as a normal host in the inventory.
It also needs be added to the `supermicro_microblade_blades` group.

This group list all blades which are going to be configured:

* in the CMM: name and IPMI settings
* on the switches: ports names and ports <-> VLANs associations


## Blade Definition

A few settings are needed to guide the Ansible playbook to compute the settings.

Here is an example you would store in `host_vars/<bladename>/blade.yml`:

```yaml
blade:
  pos: "A2"
  ipmi:
    ip: 172.24.31.21
```

Here is an example you would store in `host_vars/<bladename>/net.yml`:

```yaml
net:
  ifaces:
    0:
      untagged_vlan: 'OSAS-Provisioning'
    1:
      untagged_vlan: 'OSAS-Provisioning'
    2:
      untagged_vlan: 'OSAS-Public'
      tagged_vlans: ['OSAS-Management', 'OSAS-Internal']
    3:
      untagged_vlan: 'OSAS-Public'
      tagged_vlans: ['OSAS-Management', 'OSAS-Internal']
```

The `bladename` is used to define the hostname.

`pos` is the name of the blade slot (position on the chassis), to help locate it in case of problem.

`ipmi` defines IPMI access. `ip` is the IP on the `OSAS-Management` VLAN. The other parameters are used to setup the administrator and operator credentials. The administrator account is used by OSCI to manage the blade and parameters are defined in `group_vars/catatonic/blade.vault.yml`. The operator account may be defined to allow a tenant to have limited access to control the blade power and status; if the blade in also part of a tenant with such settings, then you would find the parameters in `group_vars/tenant_<tenantname>/blade.vault.yml`.

`ifaces` defines how network interfaces on the blade will be connected to the outside world, through the microblade network switches. For each interface you wish configured, you can define one `untagged_vlan` and a list of 802.1q `tagged_vlans`. If an interface is skipped, then no configuration will be made for it. Presently the playbook is unable to know how many interfaces are available (this could probably be done using the list of MACs, see `playbooks/supermicro_blade_setup.yml`), so you need to be extra careful with the settings and interface indexes. The index represents an interface number in hardware order.

To allow a blade to access the PXE for installing it easily, you need to setup one of its interfaces into the `OSAS-Provisioning` VLAN (untagged). Beware all interfaces cannot boot on PXE (usually the first one on a switch does ; which means eth0 on switch A1 and eth1 on switch A2 for a MBI-6128R-T2 for eg.).


## Updating Hardware Configuration

### First Time / Admin Password Modification

If the blade was never configured, then the administrator password is not yet set to the team settings, so we need to notify the playbook about it. In this case the old password is `ADMIN`.

If you want to modify the password, then change the setting `group_vars/catatonic/blade.vault.yml`.

The old/original password then needs to be set in the `OLD_IPMI_ADM_PWD` environment variable.

For a newly configured blade you would then do:

```shell
OLD_IPMI_ADM_PWD=ADMIN ansible-playbook --diff -l <bladename> -t cmm_blade_setup playbooks/supermicro_microblade_config.yml
```

You can now proceed to the rest of the changes in the following section.

### Usual Changes

When settings are ready, just fire the playbook:

```shell
 ansible-playbook --diff playbooks/supermicro_microblade_config.yml
```

You can fire only steps using these tags:

* `cmm_blade_setup`: configure settings on the CMM
* `sw_conf_gen`: generate the switches configuration files on Speedy's TFTP directory (but do not touch the switches)
* `sw_conf_apply`: load the last-generated configuration on the switches and reboot (or exclude tag `sw_reboot` to skip reboot)

