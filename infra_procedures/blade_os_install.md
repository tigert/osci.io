---
title: Blade OS Installation
---

## Accessing the Blade

To access the IPMI interface, we redirect port 443. The console display also requires port 5900.
It is not possible to bind 443 with SSH as non-root ; to avoid connecting as root we use socat to redirect the priviledge port.

```shell
ssh -L 5900:172.24.31.24:5900 -L 8443:172.24.31.24:443 root@speedy.osci.io
sudo socat tcp-listen:443,reuseaddr,fork  tcp-connect:127.0.0.1:8443
```

Before connecting, be sure to install the Java JRE (`apt get install default-jre` on Debian).

Then you can connect to the interface using `https://localhost:8443/`.

After authentication, you get this question:
```
You need the latest Java(TM) Runtime Environment. Would you like to update now?
```
to which you can simply reply `Cancel`.

From here you control the Blade's power.

Click on the console screenshot (sometimes broken) and access the Java application. After trusting the application you can now use the remote console.

When you reboot the screen size changes many times, which is very annoying, so I would suggest using a fixed-size window by disabling the `Auto-resize window` setting in the `Options->Preference->Window` menu.

Note you can also control power from the console interface.

## OS Installation

You may have to reboot and select the right menu to choose PXE. One of the interfaces of the blade should be in the `OSAS-Provisioning` VLAN and should catch DHCP and propose an installation menu. You should have access to manual and basic automated installation for classic OSes, and maybe tenant/topic-specific automated installations if there are.

If no DHCP is caught, then most probably the switches needs to be reconfigured to change the network assignation. Also beware not all blade interfaces are able to boot on PXE.

