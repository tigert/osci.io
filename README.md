osci.io Website
===============

This repository contains the content, markup, and framework for the website that appears at www.osci.io.

The goal of the website is to provide information about the services run on and who are the operators of this domain.

*

Audiences
=========
* Free/open source software contributors
.* including ones we're working with looking for links to stuff e.g. gitlab repos, docs we provide or link to, ticket system, #openinfra, etc.
* People who see *.osci.io in a pathway for one of our services e.g. OpenJDK sources and are looking for the bonafides for the service provider
* People concerned about the hand of Red Hat mucking about with stuff
* People who like #openinfra -- https://twitter.com/search?q=%23openinfra

Content overview
================
* Who we are & what we do (in general)
* Details of some of the stuff we do with an eye to those audiences
.* Services provided on this domain ...
* Docs we produce, maintain, fork, link to, etc.
* /downloads link
* manifestos aka blog planet?

Generation
==========

This website needs to be generated using Jekyll.

The syntax highlighting theme has been generated using:
    bundle exec rougify style base16.solarized.dark > assets/lib/syntax.scss

